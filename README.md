## What is Linux?

A tiny application to clear up any confusion about Linux!

## Installation
There are a few ways to install it. I found the easiest way is to simply copy it over to /usr/bin/. This will make it system-wide and all you have to do is type the command to be reminded of what Linux (as you probably know it) actually is.
